from django.shortcuts import render
from django.http import HttpResponse
from gpiozero import LED

# Create your views here.

def switch_led_view(request, *args, **kwargs):
    led = LED(2)
    if led.active_high():
        led.off()
        print('led is off.')
    else:
        led.on()
        print('led is on.')
    print(led)
    return HttpResponse('Hello!')